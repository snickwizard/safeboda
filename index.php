<?php

/**
 * Step 1: Require needed files
 */
require './libs/rest/Slim/Slim.php';
require './libs/include/config.php';
require './libs/include/Hdb.php';

/*
 * Step 2: initialize server parameters
 */

try {
//------------------start php session--
    if (!session_start()) {
        throw Exception("An error occurred while HeHengine was creating prerequisite environment");
    }
//------------------------------------------
//-------set time zone to Ugandan time--------------
    date_default_timezone_set('Africa/Kampala');

//--------------set db connection------------------
    //--------------set db connection------------------

    $db = R::setup(DB_SERVER, DB_USER, DB_PASS);

    if ($db) {
        R::freeze(true);
        R::debug(false);
    } else {
        logActivity('system', 'failed to connect to db');
    }

    \Slim\Slim::registerAutoloader();

    $hstore = new \Slim\Slim(array('routes.case_sensitive' => false,
                'debug' => true,
                'mode' => 'development'));
//-----------------init user parameters----------------
} catch (Exception $e) {

}

$hstore->get('/', function () use ($hstore) {

            $hstore->render('home.php', array('servertime' => date('jS F, Y', strtotime(date('Y-m-d H:i:s')))));
        });

$hstore->get('/test/:id', function ($id) use ($hstore) {
            try {
                print_r(getSpeedLogs($id));
            } catch (Exception $exc) {
                $exc->getMessage();
            }
        });

$hstore->get('/admin', function () use ($hstore) {

            if (isset($_SESSION['adminID']) && isset($_SESSION['username'])) {
                $totalsFromDB = getTotals();
                $totals = array('totalBodas' => $totalsFromDB['bodas'],
                    'totalTrackers' => $totalsFromDB['trackers'], 'servertime' => date('jS F, Y', strtotime(date('Y-m-d H:i:s'))));
                $hstore->render('dash.php', $totals);
            } else {
                $hstore->render('login.php');
            }
        });

$hstore->post('/admin', function () use ($hstore) {
            createUserAccount('admin', $hstore);
        });

$hstore->post('/admin/login', function () use ($hstore) {
            $Data = $hstore->request()->getBody();
            $signinData = json_decode($Data, true);
            $username = htmlentities(trim($signinData['logindata'][0]['value']));
            $passcode = htmlentities(trim($signinData['logindata'][1]['value']));
            loginAdmin($username, $passcode, $hstore);
        });

$hstore->get('/admin/:id/logout', function ($id) use ($hstore) {
            logAdminOut($id, $hstore);
        });

$hstore->put('/admin/:id/:admin', function ($id, $admin) use ($hstore) {
            $Response = array('status' => 'failed', 'details' => 'Your account  Account ' . $id . ' cannot be updated . Please try again');
            if (authorizeAdmin($admin)) {
                updateUserAccount('admin', $data);
                $Response['status'] = 'success';
            }
        });

$hstore->post('/boda/photo/:bodaID/:admin', function ($bodaID, $admin) use ($hstore) {
            $data = array('status' => 'failed', 'details' => '');
            if (authorizeAdmin($admin)) {
                if (true == uploadBodaPhoto($bodaID)) {
                    $hstore->response()->status(200);
                    $data['details'] = 'The profile image of boda of ID ' . $bodaID . ' was uploaded succesfully';
                } else {
                    $data['details'] = 'The uplad of profile image of boda of ID ' . $bodaID . ' failed';
                }
            } else {
                $hstore->response()->status(403);
                $data['details'] = 'Please log into the system under your admdinistrator account';
            }
            $hstore->response()->header('Content-Type', 'Application/json');
            $hstore->response()->setBody(json_encode($data));
        });

$hstore->post('/boda/:admin', function ($admin) use ($hstore) {

            if (authorizeAdmin($admin)) {
                createUserAccount('boda', $hstore);
            }
        });

$hstore->get('/boda/location/:boda', function ($boda) use ($hstore) {
            try {
                $data = array('status' => 'failed', 'data' => 'not found', 'Motion' => getBodaStatus($boda));
                $data['data'] = getTrackerData('boda', $boda);
                if (sizeof($data['data']) >= 1) {
                    $data['status'] = 'success';
                } else {
                    $data['data'] = 'The system could not find any GPS information about Safeboda id ' . $boda . ' No tracker has been assigned to this boda yet';
                }

                $hstore->response()->status(200);
                $hstore->response()->header('Content-Type', 'Application/json');
                $hstore->response()->setBody(json_encode($data));
            } catch (Exception $e) {
                echo $e->getMessage() . '  ' . $e->getTraceAsString();
            }
        });

$hstore->get('/edit/boda/:bodaID/:admin', function ($bodaID, $admin) use ($hstore) {

            if (authorizeAdmin($admin)) {
                $data = array('id' => $bodaID,
                    'fname' => null,
                    'lname' => null,
                    'plate_number' => null,
                    'phone_number' => null,
                    'gpsIMEI' => null,
                    'gpsSIM' => null,
                    'location' => null,
                    'totalBodas' => '',
                    'totalTrackers' => '',
                    'servertime' => date('jS F, Y', strtotime(date('Y-m-d H:i:s'))));
                $DbData = getBodaInfo('id', $bodaID);

                $data['fname'] = $DbData['first_name'];
                $data['lname'] = $DbData['last_name'];
                $data['plate_number'] = $DbData['platenumber'];
                $data['phone_number'] = $DbData['phone'];
                $data['gpsIMEI'] = $DbData['gpsimei'];
                $data['gpsSIM'] = $DbData['gpsphone'];
                $data['location'] = $DbData['location'];
                $total = getTotals();
                $data['totalBodas'] = $total['bodas'];
                $data['totalTrackers'] = $total['trackers'];
                $hstore->render('safeedit.php', $data);
            }
        });
$hstore->get('/boda/:bodaID/:admin', function ($bodaID, $admin) use ($hstore) {

            if (authorizeAdmin($admin)) {
                $BodaData = array('bodadata' => '', 'status' => 'failed');
                $BodaData['bodadata'] = getBodaInfo('id', $bodaID);
                if ($BodaData['bodadata']) {
                    $BodaData['status'] = 'success';
                }
            }
        });

$hstore->put('/boda/:id/:admin', function ($id, $admin) use ($hstore) {


            $response = array('status' => 'failed', 'details' => 'Please login in order to update boda info');
            try {
                if (authorizeAdmin($admin)) {
                    $data = array('id' => $id,
                        'fname' => null,
                        'lname' => null,
                        'plate_number' => null,
                        'phone_number' => null,
                        'gpsIMEI' => null,
                        'gpsSIM' => null,
                        'location' => null);

                    $clientBody = $hstore->request()->getBody();
                    $UpdateData = json_decode($clientBody, true);
                    $data['fname'] = htmlentities(trim((string) $UpdateData['bodadata'][0]['value']));
                    $data['lname'] = htmlentities(trim((string) $UpdateData['bodadata'][1]['value']));
                    $data['phone_number'] = htmlentities(trim((string) $UpdateData['bodadata'][2]['value']));
                    $data['gpsIMEI'] = htmlentities(trim((string) $UpdateDataa['bodadata'][3]['value']));
                    $data['plate_number'] = htmlentities(trim((string) $UpdateData['bodadata'][4]['value']));
                    $data['gpsSIM'] = htmlentities(trim((string) $UpdateData['bodadata'][5]['value']));
                    $data['location'] = htmlentities(trim((string) $UpdateData['bodadata'][6]['value']));

                    $res = updateUserAccount('boda', $data);
                    $hstore->response()->status(200);
                    $hstore->response()->header('Content-Type', 'Application/json');
                    $hstore->response()->setBody(json_encode($res));
                } else {

                    $hstore->response()->status(200);
                    $hstore->response()->header('Content-Type', 'Application/json');
                    $hstore->response()->setBody(json_encode($response));
                }
            } catch (Exception $exc) {
                $hstore->response()->status(200);
                $hstore->response()->header('Content-Type', 'Application/json');
                $response['details'] = 'something da happened while updating boda info please review this: ' + $exc->getMessage() + ' ' + $exc->getTraceAsString();
                $hstore->response()->setBody(json_encode($response));
            }
        });

$hstore->delete('/boda/:id/:admin', function ($id, $admin) use ($hstore) {

            $Response = array('status' => 'failed', 'details' => 'Boda Account ' . $id . ' cannot be deleted. Please try again');
            if (authorizeAdmin($admin)) {
                switch (deleteBodaAccount($id)) {
                    case 0:
                        $Response['status'] = 'success';
                        $Response['details'] = 'Boda Account  ' . $id . ' has been successful';
                        break;
                    case -1:
                        $Response['details'] = 'Boda Account  ' . $id . ' could not be removed from the record. Error code is BODADEL1000';
                        break;
                    case -2:
                        $Response['details'] = 'Boda removed from records but profile image of associated with his/her account ' . $id . ' could not be deleted from the system storage. Error code is BODADEL2000';
                        break;
                    case -3:
                        $Response['details'] = 'boda of account id ' . $id . ' could not be found in the system dataset . Error code is BODADEL3000';
                        break;
                    case -4:
                        $Response['details'] = 'Boda removed from records but the system could not repo GPS tracker from his/her account id  ' . $id . ' . Error code is BODADEL4000';
                        break;
                    case -5:
                        $Response['details'] = 'Something went wrong while deleting boda of  ' . $id . ' Activity has been logged. Error code is BODADEL5000';
                        break;
                    default:
                        $Response['details'] = 'Something bad and unknown happened while deleting boda ' . $id . '. This is not expected to happen';
                        break;
                }
            } else {
                $Response['details'] = 'Please login in order to attempt to delete this boda account';
            }

            $hstore->response()->status(200);
            $hstore->response()->header('Content-Type', 'Application/json');
            $hstore->response()->setBody(json_encode($Response));
        });


$hstore->get('/dash/boda/:admin/:by/:boda', function ($admin, $by, $boda) use ($hstore) {

            $data = array('status' => 'failed', 'data' => null, 'Motion' => getBodaStatus($boda));
            if (authorizeAdmin($admin)) {
                $hstore->response()->status(200);
                $data['data'] = getBodaInfo($by, $boda);
                if ($data['data']) {
                    $data['status'] = 'success';
                } else {
                    $data['data'] = 'The system could not find any GPS information about Safeboda id ' . $boda . ' No tracker has been assigned to this boda yet';
                }
            } else {
                $data['data'] = 'You need to be administrator and logged in in order to pull boda info up';
                $hstore->response()->status(403);
            }
            $hstore->response()->header('Content-Type', 'Application/json');
            $hstore->response()->setBody(json_encode($data));
        });

$hstore->get('/gps/:admin', function ($admin) use ($hstore) {
            try {
                $GPSdata = array('status' => 'failed', 'data' => 'GPS data cannot be loaded as of now');
                if (authorizeAdmin($admin)) {
                    $GPSdata['status'] = 'success';
                    $GPSdata['data'] = getTrackerData('id', 0);
                    $hstore->response()->status(200);
                } else {
                    $hstore->response()->status(403);
                    $GPSdata['data'] = 'Please login before trying to pull these data up';
                }

                $hstore->response()->header('Content-Type', 'Application/json');
                $hstore->response()->setBody(json_encode($GPSdata));
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        });

$hstore->get('/gps/info/:imei/:admin', function ($imei, $admin) use ($hstore) {
            try {

                if (authorizeAdmin($admin)) {
                    $totalsFromDB = getTotals();
                    $hstore->render('gpsinfo.php', array('imei' => $imei,
                        'totalBodas' => $totalsFromDB['bodas'],
                        'servertime' => date('jS F, Y', strtotime(date('Y-m-d H:i:s'))),
                        'totalTrackers' => $totalsFromDB['trackers']));
                } else {
                    $hstore->redirect("/");
                }
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        });

$hstore->get('/data/gps/:by/:id', function ($by, $id) use ($hstore) {
            try {
                $callback = $hstore->request()->get('callback');
                $ip = $hstore->request()->getIp();

                $GPSdata = getTrackerData($by, $id);
                $hstore->response()->status(200);
                $hstore->response()->header('Content-Type', 'Application/javascript');

                echo sprintf("%s(%s)", $callback, json_encode($GPSdata));
            } catch (Exception $exc) {
                echo $exc->getMessage();
                logActivity('system', 'There was an error while phone of ip' . $ip . ' was accessing gps data ');
            }
        });

$hstore->post('/data/gps', function () use ($hstore) {

            try {
                $clientBody = (string) $hstore->request()->getBody();
                $clientBody = str_replace('"}"', '"}', $clientBody);
                if (substr($clientBody, -1) != ']') {
                    $clientBody = "[" . $clientBody . "]";
                }
                $gpsData = json_decode($clientBody, true);

                if (sizeof($gpsData[0]['fix3']) >= 10) {
                    $gpsIMEI = trim((string) $gpsData[0]['fix3']['imei']);
                    $gpsLong = trim((string) $gpsData[0]['fix3']['longitude']);
                    $gpsLat = trim((string) $gpsData[0]['fix3']['latitude']);
                    $gpsSpeed = trim((string) $gpsData[0]['fix3']['speed']);
                    $gpsTime = trim((string) $gpsData[0]['fix3']['time']);
                    $gpsDate = trim((string) $gpsData[0]['fix3']['date']);
                } else if (sizeof($gpsData[0]['fix2']) >= 10) {
                    $gpsIMEI = trim((string) $gpsData[0]['fix2']['imei']);
                    $gpsLong = trim((string) $gpsData[0]['fix2']['longitude']);
                    $gpsLat = trim((string) $gpsData[0]['fix2']['latitude']);
                    $gpsSpeed = trim((string) $gpsData[0]['fix2']['speed']);
                    $gpsTime = trim((string) $gpsData[0]['fix2']['time']);
                    $gpsDate = trim((string) $gpsData[0]['fix2']['date']);
                } else if (sizeof($gpsData[0]['fix1']) >= 10) {
                    $gpsIMEI = trim((string) $gpsData[0]['fix1']['imei']);
                    $gpsLong = trim((string) $gpsData[0]['fix1']['longitude']);
                    $gpsLat = trim((string) $gpsData[0]['fix1']['latitude']);
                    $gpsSpeed = trim((string) $gpsData[0]['fix1']['speed']);
                    $gpsTime = trim((string) $gpsData[0]['fix1']['time']);
                    $gpsDate = trim((string) $gpsData[0]['fix1']['date']);
                }
                if (isset($gpsIMEI) && strlen($gpsIMEI) >= 11) {
                    $now = time();
                    $speedlog = array('speedVal' => round($gpsSpeed * 3600 / 1000, 4), 'time' => date('Y-m-d H:i:s'));
                    $trackerOwner = getBodaInfo('tracker', $gpsIMEI);
                    if ($trackerOwner) {
                        $owner = $trackerOwner['id'];
                    } else {
                        $owner = '0'; //will server to see GPS trackers in stray
                    }

                    $data = array('owner' => $owner,
                        'imei' => $gpsIMEI,
                        'long' => floatval(DMStoDEC($gpsLong, 'long')),
                        'lat' => floatval(DMStoDEC($gpsLat, 'lat')),
                        'speed' => $gpsSpeed,
                        'time' => $gpsTime,
                        'date' => $gpsDate);

                    if (updateGPSInfo($data) == true) {
                        $hstore->response()->status(200);
                        updateSpeeLogs($gpsIMEI, $speedlog);
                    } else {
                        $hstore->response()->status(204);
                    }
                } else {
                    logActivity('user', 'data received from GPS but not registered with database  ...data:' . $clientBody);
                    $hstore->response()->status(400);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                logActivity('system', 'system failed while updating GPS info: ' . $e->getMessage());
            }
        });

$hstore->get('/track/boda/:id/:admin', function ($id, $admin) use ($hstore) {
            try {
                if (authorizeAdmin($admin)) {
                    $bdaGPSTracker = array('lon' => ' ', 'lat' => ' ', 'time' => '', 'date' => ' ', 'motionstatus' => '', 'speed' => '', 'servertime' => date("Y-m-d H:i:s"));
                    $dbData = getTrackerData('boda', $id);
                    if ($dbData) {
                        unset($dbData['id']);
                        unset($dbData['safeID']);
                        unset($dbData['trackerID']);

                        $bdaGPSTracker['lon'] = $dbData['lon'];
                        $bdaGPSTracker['lat'] = $dbData['lat'];
                        $Motion = getBodaStatus($id);
                        $bdaGPSTracker['motionstatus'] = $Motion['details'];
                        $bdaGPSTracker['time'] = $dbData['time'];
                        $bdaGPSTracker['date'] = $dbData['gps_date'];
                        $bdaGPSTracker['speed'] = $dbData['speed'];
                    } else {
                        $bdaGPSTracker['motionstatus'] = 'boda was not found in database';
                    }
                } else {
                    $bdaGPSTracker['motionstatus'] = 'Please login into SafeBoda as administrator';
                }
                $hstore->response()->header('Content-Type', 'Application/json');
                $hstore->response()->setBody(json_encode($bdaGPSTracker));
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        });

$hstore->get('/speed/gps/:id/:admin', function ($id, $admin) use ($hstore) {
            try {
                if (authorizeAdmin($admin)) {
                    $GPSTrackerSpeedLog = array('status' => 'failed', 'logs' => ' ');
                    $dbData = array('speed' => '', 'coordinates' => '');
                    $dbData['speed'] = getSpeedLogs($id);
                    $dbData['coordinates'] = getTrackerData('imei', $id);
                    if ($dbData) {
                        $GPSTrackerSpeedLog['status'] = 'success';
                        $GPSTrackerSpeedLog['logs'] = $dbData;
                    } else {
                        $GPSTrackerSpeedLog['logs'] = 'no speed logs of this GPS tracker are available as of now';
                    }
                } else {
                    $GPSTrackerSpeedLog['logs'] = 'Please login into SafeBoda as administrator';
                }
                $hstore->response()->header('Content-Type', 'Application/json');
                $hstore->response()->setBody(json_encode($GPSTrackerSpeedLog));
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        });


$hstore->get('/data/boda/location/:lat/:lon', function ($lat, $lon) use ($hstore) {
            try {
                $callback = $hstore->request()->get('callback');
                $ip = $hstore->request()->getIp();
                $GPSdata = array('bodas' => 'All our safebodas in your area are currently busy. Please try again in 5 minutes. We are a young startup but we are growing quickly to expand our network.',
                    'status' => 'failed');

                $BodaData[] = array('fname' => '',
                    'phone' => '',
                    'plateNo' => '',
                    'image' => '',
                    'location' => '',
                    'latitude' => '',
                    'longitude' => '',
                    'distance' => '',
                    'criterion'=>'',
                    'image' => '',
                    'status' => '');

                $radius = 1.5; //1 km radius
                $GPS_bodas = getNearbyTrackers($radius, $lat, $lon);

                $bodaID[] = array('id' => '', 'status' => '', 'message' => '');
                $bodaDistance = array();
                $bodaCoordinates[] = array('lat' => '', 'long' => '');

                for ($tracker = 0; $tracker < sizeof($GPS_bodas); $tracker++) {
                    if ($GPS_bodas[$tracker]['safeID'] != 0) {
                        $bodaID[$tracker]['id'] = $GPS_bodas[$tracker]['safeID'];
                        $bodaDistance[$tracker] = round($GPS_bodas[$tracker]['distance'] * 1000, 2);
                        $bodaCoordinates[$tracker]['lat'] = $GPS_bodas[$tracker]['lat'];
                        $bodaCoordinates[$tracker]['long'] = $GPS_bodas[$tracker]['lon'];
                        $MotionStatus = getBodaStatus($GPS_bodas[$tracker]['safeID']);
                        $bodaID[$tracker]['status'] = $MotionStatus['status'];
                        $bodaID[$tracker]['message'] = $MotionStatus['details'];
                    }
                }

                //sort our bodaID array based on motion status of them
                $bodaID = record_sort($bodaID, 'status', false);

                if (sizeof($bodaID) >= 1) {
                    $bodaArray = getClosestBodas($bodaID);
                    if(sizeof($bodaArray)>=3){
                    for ($i=0;$i<3;$i++) {
                        $BodaData[$i]['fname'] = $bodaArray[$i]['first_name'];
                        $BodaData[$i]['phone'] = $bodaArray[$i]['phone'];
                        $BodaData[$i]['plateNo'] = $bodaArray[$i]['platenumber'];
                        $BodaData[$i]['image'] = $bodaArray[$i]['avatar'];
                        $BodaData[$i]['location'] = $bodaArray[$i]['location'];
                        $BodaData[$i]['latitude'] = $bodaCoordinates[$i]['lat'];
                        $BodaData[$i]['longitude'] = $bodaCoordinates[$i]['long'];
                        $BodaData[$i]['distance'] = $bodaDistance[$i];
                        $BodaData[$i]['criterion'] = $bodaID[$i]['status'];
                        $BodaData[$i]['status'] = $bodaID[$i]['message'];
                    }
                }else{
                    for ($i = 0;$i <sizeof($bodaArray);$i++) {
                        $BodaData[$i]['fname'] = $bodaArray[$i]['first_name'];
                        $BodaData[$i]['phone'] = $bodaArray[$i]['phone'];
                        $BodaData[$i]['plateNo'] = $bodaArray[$i]['platenumber'];
                        $BodaData[$i]['image'] = $bodaArray[$i]['avatar'];
                        $BodaData[$i]['location'] = $bodaArray[$i]['location'];
                        $BodaData[$i]['latitude'] = $bodaCoordinates[$i]['lat'];
                        $BodaData[$i]['longitude'] = $bodaCoordinates[$i]['long'];
                        $BodaData[$i]['distance'] = $bodaDistance[$i];
                        $BodaData[$i]['criterion'] = $bodaID[$i]['status'];
                        $BodaData[$i]['status'] = $bodaID[$i]['message'];
                    }
                }

                for($s=0;$s<=sizeof($BodaData);$s++){
                  if($BodaData[$s]['criterion']>1000){
                      unset($BodaData[$s]);
                  }
                }
                 if($BodaData[0]['fname']!=null){
                 $GPSdata['bodas'] = $BodaData;
                 $GPSdata['status'] = 'success';
                 }
                }
                $hstore->response()->status(200);
                $hstore->response()->header('Content-Type', 'text/javascript');
                echo sprintf("%s(%s)", $callback, json_encode($GPSdata));
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        });

$hstore->run();

function record_sort($records, $field, $reverse=false) {
    $hash = array();

    foreach ($records as $record) {
        $hash[$record[$field]] = $record;
    }

    ($reverse) ? krsort($hash) : ksort($hash);

    $records = array();

    foreach ($hash as $record) {
        $records [] = $record;
    }

    return $records;
}

function createUserAccount($type, $hstore) {

    $responseData = array('status' => 'failed',
        'details' => 'HeHengine encountered an error. account not created');
    try {

        $clientBody = $hstore->request()->getBody();
        $data = json_decode($clientBody, true);

        if ('boda' == $type) {

            $fname = htmlentities(trim((string) $data['bodadata'][0]['value']));
            $lname = htmlentities(trim((string) $data['bodadata'][1]['value']));
            $phone_number = htmlentities(trim((string) $data['bodadata'][2]['value']));
            $gpsIMEI = htmlentities(trim((string) $data['bodadata'][3]['value']));
            $plate_number = htmlentities(trim((string) $data['bodadata'][4]['value']));
            $gpsSIM = htmlentities(trim((string) $data['bodadata'][5]['value']));
            $location = htmlentities(trim((string) $data['bodadata'][6]['value']));

            if (strlen($gpsIMEI) >= 12) {
                if (substr($gpsIMEI, 0, 2) == 'old') {
                    $gpsIMEI = '0' . substr($gpsIMEI, -11);
                } else {
                    $gpsIMEI = trim($gpsIMEI);
                }
            }

            $date = date('Y-m-d H:i:s');
            if (strlen($fname) > 3) {
                $user = R::dispense('bodasInfo');
                $user->first_name = $fname;
                $user->last_name = $lname;
                $user->platenumber = $plate_number;
                $user->phone = $phone_number;
                $user->gpsimei = $gpsIMEI;
                $user->gpsphone = $gpsSIM;
                $user->location = $location;
                $user->reg_date = date("Y-m-d H:i:s");
                $id = R::store($user);
                if ($id) {
                    $responseData['status'] = 'success';
                    $responseData['details'] = $id;
                } else {
                    $responseData['details'] = 'This account could not be created as of now.Please try again in a moment...code 4000';
                }
            } else {
                $responseData['details'] = 'The boda name cannot be that short';
            }
        } else if ('admin' == $type) {
            $name = htmlentities(trim((string) $data['admindata'][0]['value']));
            $email = htmlentities(trim((string) $data['admindata'][1]['value']));
            $username = htmlentities(trim((string) $data['admindata'][2]['value']));
            $password = htmlentities(trim((string) $data['admindata'][3]['value']));

            if (strlen($username) > 3) {
                $user = R::dispense('authUser');
                $user->name = $name;
                $user->email = $email;
                $user->username = $username;
                $user->password = md5($password);
                $id = R::store($user);
                if ($id) {
                    $responseData['status'] = 'success';
                    $responseData['details'] = $id;
                } else {
                    $responseData['details'] = 'This account could not be created as of now.Please try again in a moment...code 4000';
                }
            } else {
                $responseData['details'] = 'This username  cannot be that short';
            }
        }

        //return JSON-encoded response body with status and details
        $hstore->response()->status(200);
        $hstore->response()->header('Content-Type', 'application/json');
        $hstore->response()->setBody(json_encode($responseData));
    } catch (Exception $e) {
        $hstore->response()->status(400);
        $hstore->response()->header('Content-Type', 'application/json');
        $hstore->response()->setBody(json_encode($responseData));
        logActivity('system', 'system failed while creating user account: ' . $e->getMessage());
    }
}

function registerBodaPhoto($boda, $photo) {
    $bodaData = R::findOne('bodasInfo', 'id=? ', array($boda));
    if ($bodaData) {
        $bodaData->avatar = $photo;
        $res = R::store($bodaData);
        if ($res) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function uploadBodaPhoto($id) {
    $accepted_imagetypes = array('image/gif', 'image/jpeg', 'image/jpg', 'image/pjpeg', 'image/x-png', 'image/png');
    $DestinationFolder = './data/boda/pictures/' . $id . '/';

    if (!file_exists($DestinationFolder)) {
        if (!mkdir($DestinationFolder, 0777, true)) {
            return false;
        }
    }


    if (!empty($_FILES["upl"])) {
        $myFile = $_FILES["upl"];
        if ($myFile["error"] !== 0) {
            return false;
        }
        $type = trim((string) $myFile["type"]);

        while ($index < sizeof($accepted_imagetypes)) {
            $mime_type = $accepted_imagetypes[$index];
            if ($mime_type == $type) {
                $foundMatch = true;
                break;
            }
            $index++;
        }

        if ($foundMatch != true) {
            return false;
        } else {
            // ensure a safe filename
            $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

            // preserve file from temporary directory
            $saveFile = move_uploaded_file($myFile["tmp_name"], $DestinationFolder . $name);
            if (!$saveFile) {
                return false;
            } else {
                // set proper permissions on the new file
                chmod($DestinationFolder . $name, 0644);
                if (registerBodaPhoto($id, $DestinationFolder . $name)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    } else {
        return false;
    }
}

function updateUserAccount($type, $data) {

    $responseData = array('status' => 'failed',
        'details' => 'An error occurred while updating account');
    try {
        if ('boda' == $type) {
            $boda = R::findOne('bodasInfo', 'id=?', array($data['id']));
            if ($boda) {
                if ($data['fname']) {
                    $boda->first_name = $data['fname'];
                }
                if ($data['lname']) {
                    $boda->last_name = $data['lname'];
                }
                if ($data['plate_number']) {
                    $boda->platenumber = $data['plate_number'];
                }
                if ($data['phone_number']) {
                    $boda->phone = $data['phone_number'];
                }
                if ($data['gpsIMEI']) {
                    $boda->gpsimei = $data['gpsIMEI'];
                }
                if ($data['gpsSIM']) {
                    $boda->gpsphone = $data['gpsSIM'];
                }
                if ($data['location']) {
                    $boda->location = $data['location'];
                }

                $id = R::store($boda);
                if ($id) {
                    $responseData['status'] = 'success';
                    $responseData['details'] = 'Account of boda ' . $id . ' has been successfully updated.';
                } else {
                    $responseData['details'] = 'This account could not be updated as of now.Please try again in a moment...code 5000';
                }
            } else {
                $responseData['details'] = 'Account of boda ' . $data['id'] . ' has not been found.';
            }
        } else if ('admin' == $type) {
            $Admin = R::find('authUser', 'id=?', array($data['id']));
            if ($Admin) {
                if ($data['name']) {
                    $Admin->name = $data['name'];
                }
                if ($data['username']) {
                    $Admin->username = $data['username'];
                }
                if ($data['email']) {
                    $Admin->email = $data['email'];
                }
                if ($data['pass']) {
                    $Admin->password = $data['pass'];
                }
                $id = R::store($Admin);
                if ($id) {
                    $responseData['status'] = 'success';
                    $responseData['details'] = 'Account of administrator ' . $id . ' has been successfully updated.';
                } else {
                    $responseData['details'] = 'This account could not be updated as of now.Please try again in a moment...code 5000';
                }
            } else {
                if (isset($data['id'])) {
                    $responseData['details'] = 'Account of administrator ' . $data['id'] . ' has not been found.';
                }
            }
        }
    } catch (Exception $e) {
        $responseData['details'] = 'An exception occurred ' . $e->getMessage();
    }
    return $responseData;
}

function deleteBodaAccount($id) {
    try {

        $boda = R::findOne('bodasInfo', 'id=?', array($id));
        if ($boda) {
            try {
                R::trash($boda);
                if (repoGPStracker($id) == false) {
                    return -4;
                }

                if (deleteDirectory('./data/boda/pictures/' . $id . '/') != 0) {
                    return-2;
                }
                return 0;
            } catch (Exception $c) {
                return -1;
            }
        } else {
            return -3;
        }
    } catch (Exception $e) {
        logActivity('user', 'An error occurred while boda ' . $id . ' being delete ' . $e->getMessage());
        return -4;
    }
}

function loginAdmin($user, $pass, $hstore) {

    $LoginDetails = array('status' => 'failed',
        'details' => 'An error occurred while loggin ');

    if (isset($user) && strlen($user) > 3 && isset($pass)) {
        $password = md5($pass);
        $SignInObject = R::findOne('authUser', 'username=? AND password=?', array($user, $password));
        if ($SignInObject) {
            $userData = $SignInObject->getProperties();
            $_SESSION['adminID'] = (string) $userData['id'];
            $_SESSION['username'] = (string) $userData['username'];
            $_SESSION['name'] = (string) $userData['name'];
            $_SESSION['email'] = (string) $userData['email'];
            if (isset($_SESSION['adminID']) && isset($_SESSION['username'])) {
                $LoginDetails['status'] = 'success';
                $LoginDetails['details'] = '/admin';
            }
        } else {
            $LoginDetails['details'] = 'login failed due to wrong credentials';
        }
    } else {
        $LoginDetails['details'] = 'login failed due to either username being too short or password not worth a try';
    }
    $hstore->response()->status(200);
    $hstore->response()->header('Content-Type', 'Application/json');
    $hstore->response()->setBody(json_encode($LoginDetails));
}

function logAdminOut($id, $hstore) {

    if (isset($_SESSION['adminID']) && $id == $_SESSION['adminID']) {
        unset($_SESSION['adminID']);
        if (isset($_SESSION['username'])) {
            unset($_SESSION['username']);
        }
        if (isset($_SESSION['name'])) {
            unset($_SESSION['name']);
        }
        if (isset($_SESSION['email'])) {
            unset($_SESSION['email']);
        }
        session_destroy();
        $hstore->redirect('/admin');
    }
}

function getAdminInfo($id) {
    try {
        if (isset($id) && $id >= 1) {
            return R::getRow('select * from authUser where  id=? ', array($id));
        } else {
            return null;
        }
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

function getBodaInfo($byWhat, $boda) {
    try {

        if (0 == $boda) { //rerun all bodas by WHat?
            if ('id' == $byWhat) {
                return R::getAll('select * from bodasInfo ORDER BY id DESC ');
            } else if ('plate' == $byWhat) {
                return R::getAll('select * from bodasInfo ORDER BY platenumber DESC ');
            } else if ('tracker' == $byWhat) {
                return R::getAll('select * from bodasInfo ORDER BY gpsimei DESC');
            }
        } else {
            if ('id' == $byWhat) {
                return R::getRow('select * from bodasInfo where  id=? ', array($boda));
            } else if ('plate' == $byWhat) {
                return R::getRow('select * from bodasInfo where  platenumber=? ', array($boda));
            } else if ('tracker' == $byWhat) {
                return R::getRow('select * from bodasInfo where  gpsimei=? ', array($boda));
            }
        }
    } catch (Exception $exc) {
        logActivity('system', 'An error occurred while getting bodas' . $exc->getMessage());
    }
}

function getClosestBodas($data) {
    try {
        $bodas = array();

        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i])) {
                $bodas[$i] = R::getRow('select * from bodasInfo where  id=? ', array($data[$i]['id']));
            }
        }

        return $bodas;
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

function updateGPSInfo($data) {

    try {
        // query tracker table for existing one
        $tracker = R::findOne('gpsinfo', 'trackerID=?', array($data['imei']));

        if ($tracker) {
            if ($data['long']) {
                $tracker->lon = $data['long'];
            }
            if ($data['lat']) {
                $tracker->lat = $data['lat'];
            }
            if ($data['speed']) {
                $tracker->speed = $data['speed'];
            }
            if ($data['time']) {
                $tracker->time = $data['time'];
            }
            if ($data['date']) {
                $tracker->gps_date = $data['date'];
            }
            if ($data['owner']) {
                $tracker->safeID = $data['owner'];
            }

            $res = R::store($tracker);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } else {
            $tracker = R::dispense('gpsinfo');
            if ($tracker) {
                $tracker->safeID = $data['owner'];
                $tracker->trackerID = $data['imei'];
                $tracker->lon = $data['long'];
                $tracker->lat = $data['lat'];
                $tracker->speed = $data['speed'];
                $tracker->time = $data['time'];
                $tracker->gps_date = $data['date'];

                $res = R::store($tracker);
                if ($res) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    } catch (Exception $e) {
        logActivity('system', 'The system crashed while update GPS  info of tracker ' . $data['imei'] . ' details are: ' . $e->getMessage());
        return false;
    }
}

function updateSpeeLogs($deviceID, $trackerData) {
    try {
        $speedDataArray = array();
        $trackerBean = R::findOne('speedlogs', 'tracker=?', array($deviceID));
        if ($trackerBean) {
            $speedData = $trackerBean->getProperties();
            $oldSpeed = json_decode($speedData['speeds'], true);
            $count = sizeof($oldSpeed);
            if ($count < 61) {
                for ($i = 0; $i < $count; $i++) {
                    array_push($speedDataArray, $oldSpeed[$i]);
                }
                array_push($speedDataArray, $trackerData);
                $speedDataArray = array_filter($speedDataArray);
                $speedDataArray = array_values($speedDataArray);
                $speed = json_encode($speedDataArray);
            } else {
                if (sizeof($oldSpeed) >= 6)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 6]);
                if (sizeof($oldSpeed) >= 5)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 5]);
                if (sizeof($oldSpeed) >= 4)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 4]);
                if (sizeof($oldSpeed) >= 3)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 3]);
                if (sizeof($oldSpeed) >= 2)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 2]);
                if (sizeof($oldSpeed) >= 1)
                    array_push($speedDataArray, $oldSpeed[sizeof($oldSpeed) - 1]);
                array_push($speedDataArray, $trackerData);
                $speedDataArray = array_filter($speedDataArray);
                $speedDataArray = array_values($speedDataArray);
                $speed = json_encode($speedDataArray);
            }
            if ($speed != null) {
                $trackerBean->speeds = $speed;
                R::store($trackerBean);
            }
        } else {
            $trackerBean = R::dispense('speedlogs');
            if ($trackerBean) {
                $trackerBean->tracker = $deviceID;
                if ($trackerData != null) {
                    $trackerBean->speeds = json_encode($trackerData);
                    R::store($trackerBean);
                }
            }
        }
    } catch (Exception $exc) {
        logActivity('system', 'An error occurred while updating speed for tracker ' . $deviceID);
    }
}

function getSpeedLogs($deviceID) {
    $data = array('time' => array(), 'speedVal' => array());
    $row = R::getRow('select speeds from speedlogs where tracker=?', array($deviceID));
    $ArrayedData = json_decode($row['speeds'], true);
    return $ArrayedData;
}

function decideBodaMotionStatus($id) {

    try {
        $boda = getBodaInfo('id', $id);
        $time = array();
        $speed = array();
        $StopSpeed = 2.12;
        $stationarySpeed = 1.94;
        if ($boda) {
            $speeds = getSpeedLogs($boda['gpsimei']);
            $speeds = array_filter($speeds);
            $speeds = array_values($speeds);
            if (sizeof($speeds) >= 1) {
                for ($i = 0; $i < sizeof($speeds); $i++) {

                    array_push($time, $speeds[$i]['time']);
                    array_push($speed, floatval($speeds[$i]['speedVal']));
                }
                $lasttime = new DateTime($time[sizeof($time) - 1]);
                $now = new DateTime(date("Y-m-d H:i:s"));
                $lastSeen = $lasttime->diff($now);
                $pastTime = $lastSeen->d;

                if ($pastTime<=1){
                    $MoreHour = $lastSeen->h;
                    if ($MoreHour <= 1) {
                        $intervalMinutes = (int) $lastSeen->i;
                        if ($intervalMinutes <= 1) {

                            //in hope array outbound won't cause problems
                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //moving
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    if ($speed[$i + 1] > $speed[$i]) {
                                        $totSpeed = $speed[$i + 1];
                                    } else {
                                        $totSpeed = $speed[$i];
                                    }
                                }
                                return '01_' . round($totSpeed / sizeof($speed), 2); //average speed
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed >= $StopSpeed) {
                                    return '01_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -0105; //highly probably no movement
                                }
                            }
                        } else if (1 < $intervalMinutes && $intervalMinutes <= 2) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //moving
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    if ($speed[$i + 1] > $speed[$i]) {
                                        $totSpeed = $speed[$i + 1];
                                    } else {
                                        $totSpeed = $speed[$i];
                                    }
                                }
                                return '02_' . round($totSpeed / sizeof($speed), 2); //average speed
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > 2.3) {
                                    return '02_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -0205; //highly probably no movement
                                }
                            }
                        } else if (2 < $intervalMinutes && $intervalMinutes <= 5) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //moving
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    if ($speed[$i + 1] > $speed[$i]) {
                                        $totSpeed = $speed[$i + 1];
                                    } else {
                                        $totSpeed = $speed[$i];
                                    }
                                }
                                return '03_' . round($totSpeed / sizeof($speed), 2); //average speed
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '03_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -0305; //highly probably no movement
                                }
                            }
                        } else if (5 < $intervalMinutes && $intervalMinutes <= 10) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //moving
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    if ($speed[$i + 1] > $speed[$i]) {
                                        $totSpeed = $speed[$i + 1];
                                    } else {
                                        $totSpeed = $speed[$i];
                                    }
                                }
                                return '05_' . round($totSpeed / sizeof($speed), 2); //average speed
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '05_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -0505; //highly probably no movement
                                }
                            }
                        } else if (10 <= $intervalMinutes && $intervalMinutes <= 20) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //moving
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    if ($speed[$i + 1] > $speed[$i]) {
                                        $totSpeed = $speed[$i + 1];
                                    } else {
                                        $totSpeed = $speed[$i];
                                    }
                                }
                                return '10_' . round($totSpeed / sizeof($speed), 2); //average speed
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > 2) {
                                    return '10_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -1005; //highly probably no movement
                                }
                            }
                        } else if (21 <= $intervalMinutes && $intervalMinutes <= 30) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //check moving pattern
                                $totSpeed = 0;
                                $n = 1;
                                $stationaryCount = 0;
                                for ($i = 0; $i < sizeof($speed); $i++) {
                                    if ($speed[$i] <= $stationarySpeed) {
                                        $stationaryCount++;
                                    }
                                    $totSpeed = $totSpeed + $speed[$i];
                                    $n = $i;
                                }
                                if (sizeof($speed) >= $stationaryCount + 5) {
                                    return '20_' . round($totSpeed / $n, 2); //return averaged speed over last 20 minutes
                                } else {
                                    return -2003; //return as stopped as from how many stationary speeds
                                }
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '20_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -2005; //highly probably no movement
                                }
                            }
                        } else if (31 <= $intervalMinutes && $intervalMinutes <= 40) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //check moving pattern
                                $totSpeed = 0;
                                $n = 1;
                                $stationaryCount = 0;
                                for ($i = 0; $i < sizeof($speed); $i++) {
                                    if ($speed[$i] <= $stationarySpeed) {
                                        $stationaryCount++;
                                    }
                                    $totSpeed = $totSpeed + $speed[$i];
                                    $n = $i;
                                }
                                if (sizeof($speed) >= $stationaryCount + 8) {
                                    return '30_' . round($totSpeed / $n, 2); //return averaged speed over last 30 minutes
                                } else {
                                    return -3003;
                                }
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '30_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -3005; //highly probably no movement
                                }
                            }
                        } else if (41 <= $intervalMinutes && $intervalMinutes <= 50) {

                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //check moving pattern
                                $totSpeed = 0;
                                $n = 1;
                                $stationaryCount = 0;
                                for ($i = 0; $i < sizeof($speed); $i++) {
                                    if ($speed[$i] <= $stationarySpeed) {
                                        $stationaryCount++;
                                    }
                                    $totSpeed = $totSpeed + $speed[$i];
                                    $n = $i;
                                }
                                if (sizeof($speed) >= $stationaryCount + 11) {
                                    return '40_' . round($totSpeed / $n, 3); //return averaged speed over last 40 minutes
                                } else {
                                    return -4003;
                                }
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '40_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -4005; //highly probably no movement
                                }
                            }
                        } else if (51 <= $intervalMinutes && $intervalMinutes <= 60) {
                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //check moving pattern
                                $totSpeed = 0;
                                $n = 1;
                                $stationaryCount = 0;
                                for ($i = 0; $i < sizeof($speed); $i++) {
                                    if ($speed[$i] <= $stationarySpeed) {
                                        $stationaryCount++;
                                    }
                                    $totSpeed = $totSpeed + $speed[$i];
                                    $n = $i;
                                }
                                if (sizeof($speed) >= $stationaryCount + 15) {
                                    return '50_' . round($totSpeed / $n, 2);  //return averaged speed over last 60 minutes
                                } else {
                                    return -5003;
                                }
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '50_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -5005; //highly probably no movement
                                }
                            }
                        } else {
                            if ($speed[sizeof($speed) - 1] >= $StopSpeed && $speed[sizeof($speed) - 2] >= $StopSpeed && $speed[sizeof($speed) - 3] >= $StopSpeed && $speed[sizeof($speed) - 4] >= $StopSpeed && $speed[sizeof($speed) - 5] >= $StopSpeed) { //check moving pattern
                                $totSpeed = 0;
                                $n = 1;
                                $stationaryCount = 0;
                                for ($i = 0; $i < sizeof($speed); $i++) {
                                    if ($speed[$i] <= $stationarySpeed) {
                                        $stationaryCount++;
                                    }
                                    $totSpeed = $totSpeed + $speed[$i];
                                    $n = $i;
                                }
                                if (sizeof($speed) >= $stationaryCount + 18) {
                                    return $intervalMinutes . '_' . round($totSpeed / $n, 2);  //return averaged speed over an hour ago
                                } else {
                                    return -6003; //decide as stopped as from how many stops
                                }
                            } else {
                                //
                                $totSpeed = 0;
                                for ($i = sizeof($speed) - 5; $i < sizeof($speed) - 1; $i++) {
                                    $totSpeed += $speed[$i];
                                }
                                $meanSpeed = round($totSpeed / sizeof($speed), 2);
                                if ($meanSpeed > $stationarySpeed) {
                                    return '60_' . $meanSpeed; //return max speed
                                } else {
                                    //
                                    return -6005; //highly probably no movement
                                }
                            }
                        }
                    } else {
                        $lasttime = null;
                        $now = null;
                        return 'hour_' . $MoreHour;
                    }
                } else {
                    $lasttime = null;
                    $now = null;
                    return 'day_' . $pastTime; //boda has no record of moving today
                }
            } else {
                return -2; //boda turned the tracker off
            }
        } else {
            return -3; //boda never registered
        }
    } catch (Exception $exc) {
        echo $exc->getMessage() . ' Trace ' . $exc->getTraceAsString();
    }
}

function getBodaStatus($bodaID) {
    $StatusData = array('status' => '0', 'speed' => '0', 'details' => 'Not defined ');
    $BodaName = getBodaInfo('id', $bodaID);
    $name = $BodaName['first_name'];
    $status = decideBodaMotionStatus($bodaID);

    if ($status < 0) {
        //TO RENDER nearby bodas we need to sort them by this status value by increasing order.
        //therefore bodas with larger status will be picked lat
        switch ($status) {
            case -1:
                $StatusData['status'] = 10000;
                $StatusData['details'] = $name . ' has not moved in a day';
                break;
            case -2:
                $StatusData['status'] =12000;
                $StatusData['details'] = $name . ' has no moving history. Please check them out later.';
                break;
            case -3:
                $StatusData['status'] = 1100000;
                $StatusData['details'] = $name . ' is not affiliated with SafeBoda and you should not be getting this.';
                break;
            case -0105:
                $StatusData['status'] = 101;
                $StatusData['details'] = $name . ' has not moved as of a minute ago';
                break;
            case -0205:
                $StatusData['status'] = 102;
                $StatusData['details'] = $name . ' has not moved as of 2 minutes ago';
                break;
            case -0305:
                $StatusData['status'] = 103;
                $StatusData['details'] = $name . ' has not moved as of 3 minutes ago';
                break;
            case -0505:
                $StatusData['status'] = 105;
                $StatusData['details'] = $name . ' has not moved as of 5 minutes ago';
                break;
            case -1005:
                $StatusData['status'] = 110;

                $StatusData['details'] = $name . ' has not moved as of 10 minutes';
                break;
            case -2003:
                $StatusData['status'] = 220;
                $StatusData['details'] = $name . ' was moving  as of last 20 minutes but had suspiciously many stops.Hard to tell if there is a passenger onboard';
                break;
            case -2005:
                $StatusData['status'] = 120;
                $StatusData['details'] = $name . ' has not moved as of 3 minutes ago';
                break;
            case -3003:
                $StatusData['status'] = 230;
                $StatusData['details'] = $name . ' was moving as of last 30 minutes but had suspiciously many stops.Hard to tell if there is a passenger onboar';
                break;
            case -3005:
                $StatusData['status'] = 130;
                $StatusData['details'] = $name . '  is free as of 30 minutes ago';
                break;
            case -4003:
                $StatusData['status'] = 240;
                $StatusData['details'] = $name . ' was moving as of last 40 minutes but had suspiciously many stops.Hard to tell if there is a passenger onboar';
                break;
            case -4005:
                $StatusData['status'] = 140;
                $StatusData['details'] = $name . ' is free as of 40 minutes ago';
                break;
            case -5003:
                $StatusData['status'] = 250;
                $StatusData['details'] = $name . ' was moving as of last 50 minutes but had suspiciously many stops.Hard to tell if there is a passenger onboar';
                break;
            case -5005:
                $StatusData['status'] = 526;
                $StatusData['details'] = $name . '  is free as of 50 minutes ago';
                break;
            case -6003:
                $StatusData['status'] = 560;
                $StatusData['details'] = $name . ' was moving an hour ago but had suspiciously many stops.Hard to tell if there is a passenger onboard';
                break;
            case -6005:
                $StatusData['status'] = 660;
                $StatusData['details'] = $name . '  is free as of more than an hour ago';
                break;
            default:
                $StatusData['status'] = 1000;
                $StatusData['details'] = 'System could not determine boda status Boda ';
                break;
        }
    } else {
        $MovingStatus = explode('_', $status);
        if ($MovingStatus) {
            if ($MovingStatus[0] == 'day') {
                $StatusData['status'] = 2000;
                $StatusData['speed'] = '0';
                $StatusData['details'] = $name . ' has not move in (' . $MovingStatus[1] . ') days ';
            } else if ($MovingStatus[0] == 'hour') {
                $StatusData['status'] = 1500;
                $StatusData['speed'] = '0';
                $StatusData['details'] = $name . ' has not move in (' . $MovingStatus[1] . ') hours ';
            } else {
                if ($MovingStatus[1] <= 4.01) {
                    $StatusData['status'] = 170;
                    $StatusData['details'] = $name . ' is seemingly taking a stroll around with an average speed of  ' . $MovingStatus[1] . ' m/s';
                } else {
                    $StatusData['status'] = 1000;
                    $StatusData['speed'] = $MovingStatus[1];
                    $StatusData['details'] = $name . ' is busy  in last ' . $MovingStatus[0] . ' minutes. Travelling at average speed of ' . $MovingStatus[1] . ' m/s';
                }
            }
        }
    }

    return $StatusData;
}

function repoGPStracker($bodaID) {
    $tracker = R::findOne('gpsinfo', 'safeID=?', array($bodaID));
    if ($tracker) {
        $tracker->safeID = 0;
        $yes = R::store($tracker);
        if ($yes) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function DMStoDEC($dms, $longlat) {

    if ($longlat == 'lat') {
        $deg = substr($dms, 0, 2);
        $min = substr($dms, 2, 7);
        $sec = '';
        if (strtolower(substr($dms, -1)) == 's') {
            $lat = $deg + ((($min * 60) + ($sec)) / 3600);
            return "-" . $lat;
        } else {
            return $deg + ((($min * 60) + ($sec)) / 3600);
        }
    }
    if ($longlat == 'long') {
        $deg = substr($dms, 0, 3);
        $min = substr($dms, 3, 9);
        $sec = '';
        if (strtolower(substr($dms, -1)) == 'w') {
            $long = $deg + ((($min * 60) + ($sec)) / 3600);
            return "-" . $long;
        } else {
            return $deg + ((($min * 60) + ($sec)) / 3600);
        }
    }
}

function getTrackerData($by, $id) {
    $data = array();
    if ($by == 'boda') {
        $data = R::getRow('select *  from gpsinfo  where safeID=?', array($id));
    } else if ($by == 'imei') {
        $data = R::getRow('select *  from gpsinfo  where trackerID=?', array($id));
    } else {
        if ($id == 0) {
            $data = R::getAll('select *  from gpsinfo');
        } else {
            $data = R::getRow('select *  from gpsinfo  where id=?', array($id));
        }
    }

    if (strlen($data['gps_date']) >= 5) {
        if (substr(trim($data['trackerID']), 0, 1) == '0') {
            $date = substr($data['gps_date'], 0, 2) . '-' . substr($data['gps_date'], -4, 2) . '-' . substr($data['gps_date'], -2);  //old gps tracker format
        } else {
            $date = substr($data['gps_date'], -2) . '-' . substr($data['gps_date'], -4, 2) . '-' . substr($data['gps_date'], 0, 2);
        }
        $data['gps_date'] = date('jS F, Y', strtotime($date));
    }

    if (strlen($data['time']) >= 4) {
        $time = substr($data['time'], 0, 2) . ':' . substr($data['time'], 2, 2) . ':' . substr($data['time'], -2);
        $data['time'] = $time;
    }
    if ((float) $data['speed'] != 0) {
        $speed = (float) $data['speed'] * 1000 / 3600;
        $data['speed'] = round($speed, 2);
    }
    return $data;
}

function getNearbyTrackers($radius, $lat, $long) {
    try {

        $query = "SELECT safeID,lat,lon, ( 6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( lat ) ) * cos( radians( lon ) - radians(" . $long . ") ) + sin( radians(" . $lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM gpsinfo HAVING distance <" . $radius . " AND safeID !=0 ORDER BY distance LIMIT 10";

        $locations = R::getAll($query);

        if ($locations) {
            return $locations;
        } else {
            return"null";
        }
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

function authorizeAdmin($id) {
    if (isset($_SESSION['adminID']) && $id == $_SESSION['adminID']) {
        return true;
    } else {
        return false;
    }
}

function getTotals() {
    try {
        $totals = array('bodas' => '',
            'trackers' => '');
        $totals['bodas'] = R::getCell("select COUNT(*)  from bodasInfo");
        $totals['trackers'] = R::getCell("select COUNT(*)  from gpsinfo");
        return $totals;
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

function deleteDirectory($dirPath) {
    if (is_dir($dirPath)) {
        $objects = scandir($dirPath);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                    deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                } else {
                    unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
        reset($objects);
        if (rmdir($dirPath)) {
            return 0;
        } else {
            return -2;
        }
    } else {
        return -3;
    }
}

function logActivity($type, $logMsg) {

    $file = date('Y-m-d') . '-Hlog.txt';

    $dir = './safeserver/logs/default';

    if ($type == 'system') {
        $logMsg = 'System: ' . $logHeader . $logMsg . "\n";
        $dir = './safeserver/logs/system';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                die();
            }
        } else {
            $log = "\n" . date('Y-m-d H:i:s') . ': ' . $logMsg . "\n";
            $logStatus = file_put_contents($dir . '/' . $file, $log, FILE_APPEND | LOCK_EX);
            if ($logStatus === false) {
                die();
            }
        }
    } else if ($type == 'user') {
        $logMsg = 'User: ' . $logHeader . $logMsg . "\n";
        $dir = './safeserver/logs/user';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                die();
            }
        } else {

            $log = "\n" . date('Y-m-d H:i:s') . ': ' . $logMsg . "\n";
            $logStatus = file_put_contents($dir . '/' . $file, $log, FILE_APPEND | LOCK_EX);
            if ($logStatus === false) {
                die();
            }
        }
    } else {
        $logMsg = 'dump: ' . $logHeader . $logMsg . '\n';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                die();
            }
        } else {

            $log = "\n" . date('Y-m-d H:i:s') . ': ' . $logMsg . "\n";
            $logStatus = file_put_contents($dir . '/' . $file, $log, FILE_APPEND | LOCK_EX);
            if ($logStatus === false) {
                die();
            }
        }
    }
}

?>